<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo 'Olá mundo!<br>';

//comentario de linha

/* 
comentario 
de 
bloco
*/

//$ variável
$nome = 'Letícia Lima';
echo'olá, $nome <br>';
echo'olá, ' . $nome . '! <br>'; //concatenação
echo"olá, $nome!";//interpolação

//estrutura condicional:
//IF
if($nome == 'Letícia'){
    echo'<br>O nome igual<br>';
}else{
    echo'<br>O nome é diferente<br>';
}

//SWITCH
$dia = 'sábado';
switch($dia){
    case 'segunda':
        echo'estude';
        break;
    case 'terça':
        echo'vá para aula de CMS';
        break;
    case 'quarta':
        echo'vá para aula de BD';
        break;
    case 'quinta':
        echo'venha pra cá';
        break;
    case 'sexta':
        echo'vá para aula de SCRIPT PARA WEB';
        break;
    default:
        echo'vá descansar';  
}
echo'<br>';

//OPERADOR TERNÁRIO
$animal = 'cachorro';
$tipo = $animal == 'cachorro' ? 'maméfero' : 'desconhecido';
echo"$animal é um animal do tipo $tipo";

echo'<br>';

//$sobrenome_informado = 'Higino';
$sobrenome = $sobrenome_informado ?? 'não informado';
echo"sobrenome: $sobrenome";

echo'<br>';

//estrutura de repetição:
//FOR
for($i = 0; $i < 10; $i++){
    echo"Essa é a linha $i <br>";
}

echo'<br>';

//WHILE
$i = 0;
while($i < 10){
    echo"Essa é a linha $i <br>";
    $i++;
}


//COMO CHAMAR OUTROS CÓDIGOS
include 'link.html'; //se não encontrar esse arquivo da um erro, mas o código continua a ser executado
require 'link.html'; // se não encontar da um erro fatal, ou seja, para ai mesmo
include_once 'link.html'; // checa se esse link já foi incluido se sim não é executado
require_once 'link.html'; // checa se esse link já foi incluido se sim não é executado